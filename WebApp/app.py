
from flask import Flask, render_template, request, url_for, redirect
import os

from WebApp.process.read_csv import as_dictionary, as_list, as_dictionary_timestamp
from WebApp.process.control_flow import create_control_flow
from WebApp.process.handover_of_work import create_handover
from WebApp.process.parse_timestamp import create_parse_timestamp
from WebApp.process.dotted_chart import create_dotted_chart
from WebApp.process.activity_execution_time import create_histogram
from WebApp.process.parsing_xes import parsing_xes
from WebApp.process.working_together import create_graph
from WebApp.process.distribution_of_work import create_distribution

app = Flask(__name__)


# Define route
# Index route
@app.route('/')
def index():
    # Checking if file is already been uploaded
    if os.path.isfile('./static/data/event.xes'):
        # Check if xes file is already been parsed to csv
        if not(os.path.isfile('./static/data/event.csv')):
            parsing_xes('./static/data/event.xes')
        return redirect('display_data', code=302)
    elif os.path.isfile('./static/data/event.csv'):
        return redirect('display_data', code=302)

    return render_template('upload.html', title='', upload=False)


# Route for displaying the data that has been uploaded
@app.route('/display_data', methods=['POST', 'GET'])
def display_data():
    active = request.path

    try:
        # If the form is running
        type = request.form['select_data']
        log = []

        # Checking which data is being requested
        if type == 'data_a':
            if os.path.isfile('./static/data/datasetA.csv'):
                log = as_list('./static/data/datasetA.csv')

        if type == 'data_o':
            if os.path.isfile('./static/data/datasetO.csv'):
                log = as_list('./static/data/datasetO.csv')

        if type == 'data_w':
            if os.path.isfile('./static/data/datasetW.csv'):
                log = as_list('./static/data/datasetW.csv')

        log = as_list('./static/data/event.csv')
        return render_template('data.html', title='', active=active, log=log, uploaded=True)

    except Exception as e:
        print(e)

    # If the form is not running
    if os.path.isfile('./static/data/event.xes') or os.path.isfile('./static/data/event.csv'):
        return render_template('display_data.html', title='', active=active, uploaded=True)

    # If user has not uplaoded the file
    return redirect('', code=302)


# Route for displaying the main menu to choose the graph that we wanted
# FIXME: setiap membuat graph yang sama dengan data yang berbeda, yang tertampil graph yang pertama kali dibuat
@app.route('/graph', methods=['POST', 'GET'])
def graph():
    # FIXME: masih error buat jalanin control flow pake data yang buat skripsi
    active = request.path

    try:
        graph_type = request.form['graph_type']
        data = request.form['select_data']
        log = dict()
        file = None
        # TODO: tambahin menu buat batesin jumlah data yang mau digunakan

        # Check which data is requested
        if data == 'data_a':
            if os.path.isfile('./static/data/datasetA.csv'):
                file = './static/data/datasetA.csv'

        if data == 'data_w':
            if os.path.isfile('./static/data/datasetW.csv'):
                file = './static/data/datasetW.csv'

        if data == 'data_o':
            if os.path.isfile('./static/data/datasetO.csv'):
                file = './static/data/datasetO.csv'

        file = './static/data/event.csv'
        # Check which graph is requested
        # Control flow graph
        if graph_type == 'control_flow':
            log = as_dictionary(file)

            # Deleting previous image if exist
            if os.path.isfile('./static/img/control_flow.png'):
                os.unlink('./static/img/control_flow.png')

            create_control_flow(log, './static/img/control_flow.png')
            filename = 'img/control_flow.png'
            return render_template('display_graph.html', title='Control Flow', active=active, uploaded=True, filename=filename)

        # Handover of work graph
        if graph_type == 'handover_of_work':
            log = as_dictionary(file)

            if os.path.isfile('./static/img/handover_of_work.png'):
                os.unlink('./static/img/handover_of_work.png')

            create_handover(log, './static/img/handover_of_work.png')
            filename = 'img/handover_of_work.png'
            return render_template('display_graph.html', title='Handover of Work', active=active, uploaded=True, filename=filename)

        # Parse timestamp graph
        if graph_type == 'parse_timestamp':
            log = as_dictionary_timestamp(file)

            if os.path.isfile('./static/img/parse_timestamp.png'):
                os.unlink('./static/img/parse_timestamp.png')

            create_parse_timestamp(log, './static/img/parse_timestamp.png')
            filename = 'img/parse_timestamp.png'
            return render_template('display_graph.html', title='Parse Timestamp', active=active, uploaded=True, filename=filename)

        # Dotted chart graph
        # FIXME: data yang lama ikut terpakai, contoh: pertama menggunakan data A keluar graph A, kedua menggunakan data O keluar graph A dan O
        if graph_type == 'dotted_chart':
            log = as_dictionary_timestamp(file)

            if os.path.isfile('./static/img/dotted_chart.png'):
                os.unlink('./static/img/dotted_chart.png')

            create_dotted_chart(log, './static/img/dotted_chart.png')
            filename = 'img/dotted_chart.png'
            return render_template('display_graph.html', title='Dotted Chart', active=active, uploaded=True, filename=filename)

        # Histogram graph
        # FIXME: ketika menggunakan bagian ini sering terjadi error NULL main window dan main thread is not in main loop
        if graph_type == 'activity_execution_time':
            log = as_dictionary_timestamp(file)

            if os.path.isfile('./static/img/activity_execution_time.png'):
                os.unlink('./static/img/activity_execution_time.png')

            create_histogram(log, './static/img/activity_execution_time.png')
            filename = 'img/activity_execution_time.png'
            return render_template('display_graph.html', title='Activity Execution Time Histogram', active=active, uploaded=True, filename=filename)

        # Working together graph
        if graph_type == 'working_together':
            log = as_dictionary(file)

            if os.path.isfile('./static/img/working_together.png'):
                os.unlink('./static/img/working_together.png')

            create_graph(log, './static/img/working_together.png')
            filename = 'img/working_together.png'
            # FIXME: yang complete dibenerin lagi, belum bener ngasih parameternya
            return render_template('display_graph.html', title='Working Together', active=active, uploaded=True, filename=filename)

        # Distribution of work graph
        if graph_type == 'distribution_of_work':
            log = as_dictionary(file)

            if os.path.isfile('./static/img/distribution_of_work.png'):
                os.unlink('./static/img/distribution_of_work.png')

            create_distribution(log, './static/img/distribution_of_work.png')
            filename = 'img/distribution_of_work.png'
            # FIXME: file
            return render_template('display_graph.html', title='Distribution of Work', active=active, uploaded=True, filename=filename)

    except Exception as e:
        print(type(e))
        print(e)

    if os.path.isfile('./static/data/event.csv'):
        return render_template('graph.html', title='', active=active, uploaded=True)

    return redirect('', code=302)


# Route for handling the data upload xes
@app.route('/upload', methods=['POST'])
def upload():
    # TODO: cek tipe file apa, dihandle misal kalo xes di parse, kalo csv gausah
    # Cek apakah folder data sudah ada
    if not os.path.isdir('./static/data'):
        os.makedirs('./static/data')

    # Mengambil file dari request http
    file = request.files['file']
    print(str(file))

    # Mengecek apakah file berekstensi xes
    if file.filename.endswith('.xes'):
        print(os.path.join('./static/data', 'event.xes'))
        file.save(os.path.join('./static/data', 'event.xes'))
    # Mengecek apakah file berekstensi csv
    elif file.filename.endswith('.csv'):
        print(os.path.join('./static/data', 'event.csv'))
        file.save(os.path.join('./static/data', 'event.csv'))
    else:
        print('Jenis file salah')

    return redirect('', code=302)


# Route for deleting the data
@app.route('/delete', methods=['POST'])
def delete():
    # Checking if the file exist
    if os.path.isfile('./static/data/event.xes'):
        os.unlink('./static/data/event.xes')
    if os.path.isfile('./static/data/event.csv'):
        os.unlink('./static/data/event.csv')
    if os.path.isfile('./static/data/datasetA.csv'):
        os.unlink('./static/data/datasetA.csv')
    if os.path.isfile('./static/data/datasetO.csv'):
        os.unlink('./static/data/datasetO.csv')
    if os.path.isfile('./static/data/datasetW.csv'):
        os.unlink('./static/data/datasetW.csv')

    return redirect('', code=302)


app.run(debug=True)
