import pygraphviz as pgv


def create_graph(log, file_path):
    W = dict()
    for case_id in log:
        S = set()
        for i in range(0, len(log[case_id])):
            ui = log[case_id][i][2]
            if ui != '':
                # Handle for data W, where it's event_type needs to be COMPLETE
                if file_path == './static/img/working_together.png':
                    if log[case_id][i][1] != 'COMPLETE':
                        continue
                S.add(ui)
        S = sorted(list(S))
        for i in range(0, len(S) - 1):
            for j in range(i + 1, len(S)):
                ui = S[i]
                uj = S[j]
                if ui not in W:
                    W[ui] = dict()
                if uj not in W[ui]:
                    W[ui][uj] = 0
                W[ui][uj] += 1

    counter = 0
    for ui in sorted(W.keys()):
        for uj in sorted(W[ui].keys()):
            # print(ui, '--', uj, ':', W[ui][uj])
            counter += 1
            # print(counter)

    G = pgv.AGraph(strict=False, directed=False)

    G.graph_attr['rankdir'] = 'LR'
    G.node_attr['shape'] = 'circle'

    values = [W[ui][uj] for ui in W for uj in W[ui]]
    print(values)
    x_min = min(values)
    x_max = max(values)

    y_min = 1.0
    y_max = 5.0

    for ui in W:
        for uj in W[ui]:
            # Minimum of 300
            if W[ui][uj] >= 300:
                # print(ui, '--', uj, ':', W[ui][uj])
                x = W[ui][uj]
                y = y_min + (y_max - y_min) * (float(x - x_min) / float(x_max - x_min)) ** 3
                G.add_edge(ui, uj, label=x, penwidth=y)

    G.draw(file_path, prog='circo')



