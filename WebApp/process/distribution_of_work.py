import pygraphviz as pgv


def create_distribution(log, file_path):
    UA = dict()
    for case_id in log:
        for i in range (0, len(log[case_id])):
            ai = log[case_id][i][0]
            ui = log[case_id][i][2]
            if ui not in UA:
                UA[ui] = dict()
            if ai not in UA[ui]:
                UA[ui][ai] = 0
            UA[ui][ai] += 1

    G = pgv.AGraph(strict=False, directed=False)

    G.graph_attr['rankdir'] = 'LR'
    G.node_attr['shape'] = 'circle'

    values = [UA[ui][ai] for ui in UA for ai in UA[ui]]
    x_min = min(values)
    x_max = max(values)

    y_min = 1.0
    y_max = 5.0

    for ui in UA:
        for ai in UA[ui]:
            x = UA[ui][ai]
            if x >= 1000:
                y = y_min + (y_max-y_min) * float(x-x_min) / float(x_max-x_min)
                print(x)
                print(y)
                print(ui)
                print(ai)
                print('\n')
                if ui != '':
                    G.add_edge(ui, ai, label=x, penwidth=y)

    G.draw(file_path, prog='dot')
