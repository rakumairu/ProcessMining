import pygraphviz as pgv


def create_handover(log, path_file):
    # Listing 11 dengan perbedaan pada line ke 4 dan 5
    # ui dan uj merepresentasikan dua user
    H = dict()
    for case_id in log:
        for i in range(0, len(log[case_id])-1):
            ui = log[case_id][i][2]
            uj = log[case_id][i+1][2]
            if ui not in H:
                H[ui] = dict()
            if uj not in H[ui]:
                H[ui][uj] = 0
            H[ui][uj] += 1

    # Listing 15
    G = pgv.AGraph(strict=False, directed=True)

    G.graph_attr['rankdir'] = 'LR'
    G.node_attr['shape'] = 'circle'

    values = [H[ui][uj] for ui in H for uj in H[ui]]
    x_min = min(values)
    x_max = max(values)

    y_min = 1.0
    y_max = 5.0

    for ui in H:
        for uj in H[ui]:
            # Took too much time if there is no minimum value
            # TODO: harus menggunakan batas minimum agar tidak memakan banyak waktu, tapi tidak ditentukan batasnya di buku
            x = H[ui][uj]
            y = y_min + (y_max-y_min) * float(x-x_min) / float(x_max-x_min)
            G.add_edge(ui, uj, label=x, penwidth=y)

    G.draw(path_file, prog='dot')