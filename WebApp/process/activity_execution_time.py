import numpy as np
import matplotlib.pyplot as plt
import datetime


def create_histogram(log, file_path):
    # D = dict()
    # for case_id in log:
    #     for i in range(0, len(log[case_id]) - 1):
    #         (ai, _, _, ti) = log[case_id][i]
    #         (aj, _, _, tj) = log[case_id][i + 1]
    #         if ai not in D:
    #             D[ai] = dict()
    #         if aj not in D[ai]:
    #             D[ai][aj] = []
    #         D[ai][aj].append(tj - ti)
    #
    # for ai in sorted(D.keys()):
    #     for aj in sorted(D[ai].keys()):
    #         sum_td = sum(D[ai][aj], datetime.timedelta(0))
    #         count_td = len(D[ai][aj])
    #         avg_td = sum_td / count_td
    #         avg_td -= datetime.timedelta(microseconds=avg_td.microseconds)
    #         D[ai][aj] = avg_td
    #         print(ai, '->', aj, ':', D[ai][aj])

    D = dict()

    for case_id in log:
        for i in range(0, len(log[case_id])):
            (a, __, __, t) = log[case_id][i]
            if i > 0:
                (a, _, __, t0) = log[case_id][i - 1]
                d = (t - t0).total_seconds() / (24 * 3600)
            else:
                d = 0.
            if a not in D:
                D[a] = []
            D[a].append(d)

    # FIXME: masih bingung setting histogramnya biar kalau data besar ga kecil2 banget gambarnya
    nrows = 4
    ncols = 2

    fig, ax = plt.subplots(nrows, ncols)
    # fig, ax = plt.subplots()

    i = 0
    j = 0
    for a in sorted(D.keys()):
        print('%s: mean=%.2f std=%.2f days' % (a, np.mean(D[a]), np.std(D[a])))
        ax[i, j].hist(D[a], bins=[0.1 * k for k in range(100)])
        ax[i, j].set_title(a)
        ax[i, j].set_xticks(range(10))
        ax[i, j].grid(True)
        if i == nrows - 1:
            ax[i, j].set_xlabel('days')
        j += 1
        if j == ncols:
            i += 1
            j = 0

    # plt.tight_layout()
    # plt.show()
    plt.savefig(file_path)