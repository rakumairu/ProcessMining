import matplotlib.pyplot as plt
import datetime


def create_dotted_chart(log, file_path):
    # Listing 32
    D = dict()
    for case_id in log:
        for i in range(0, len(log[case_id])-1):
            (ai, _, _, ti) = log[case_id][i]
            (aj, _, _, tj) = log[case_id][i+1]
            if ai not in D:
                D[ai] = dict()
            if aj not in D[ai]:
                D[ai][aj] = []
            D[ai][aj].append(tj-ti)

    for ai in sorted(D.keys()):
        for aj in sorted(D[ai].keys()):
            sum_td = sum(D[ai][aj], datetime.timedelta(0))
            count_td = len(D[ai][aj])
            avg_td = sum_td/count_td
            avg_td -= datetime.timedelta(microseconds=avg_td.microseconds)
            D[ai][aj] = avg_td
            print(ai, '->', aj, ':', D[ai][aj])

    X = dict()
    Y = dict()

    case_ids = sorted(log.keys(), key=lambda case_id: log[case_id][0][-1])

    for (y, case_id) in enumerate(case_ids):
        for i in range(0, len(log[case_id])):
            (a, _, _, x) = log[case_id][i]
            if a not in X:
                X[a] = []
                Y[a] = []
            X[a].append(x)
            Y[a].append(y)

    for a in sorted(X.keys()):
        plt.plot(X[a], Y[a], 'o', label=a, markersize=20, markeredgewidth=0., alpha=0.5)

    axes = plt.gca()

    axes.set_yticks(range(len(case_ids)))
    axes.set_ylim(-1, len(case_ids))
    axes.set_yticklabels(case_ids)
    axes.set_ylabel('case id')
    axes.invert_yaxis()

    axes.set_xlabel('timestamp')
    axes.xaxis.tick_top()
    axes.xaxis.set_label_position('top')

    plt.grid(True)
    plt.legend(numpoints=1)
    # plt.tight_layout()
    # plt.show()
    plt.savefig(file_path)
