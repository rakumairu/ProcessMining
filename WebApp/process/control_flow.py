"""
   Reading event log as list
"""

import pygraphviz as pgv


def create_control_flow(log, file_path):
    """
    Membuat control flow diagram dengan jumlah transition dan jumlah aktivitas dari data log
    :param log: data yang digunakan untuk dibuat menjadi diagram dengan tipe data dictionary
    :param file_path: path untuk file diagram yang dibuat
    """
    F = dict()

    # Listing 11
    for case_id in log:
        for i in range(0, len(log[case_id]) - 1):
            # Mengambil task dari i
            ai = log[case_id][i][0]

            # Mengambil task dari task selanjutnya
            aj = log[case_id][i + 1][0]

            if ai not in F:
                F[ai] = dict()
            if aj not in F[ai]:
                F[ai][aj] = 0

            # Menghitung transition dari task ai dan aj yang berhubungan
            F[ai][aj] += 1

    # Menghitung activity counts, Listing 16
    A = dict()
    for case_id in log:
        for i in range(0, len(log[case_id])):
            ai = log[case_id][i][0]
            if ai not in A:
                A[ai] = 0
            A[ai] += 1

    # Membuat graph, Listing 14
    G = pgv.AGraph(strict=False, directed=True)

    # Menentukan orientasi dari graphnya
    G.graph_attr['rankdir'] = 'LR'
    # Menentukan bentuk dari graph
    G.node_attr['shape'] = 'box'

    # Menentukan nilai warna max dan min, Listing 18
    x_min = min(A.values())
    x_max = max(A.values())

    # Memasukkan activity count dalam pembuatan graph, Listing 16
    for ai in A:
        # Label yang akan digunakan pada node graph
        text = ai + '\n(' + str(A[ai]) + ')'

        # Menambahkan warna pada node dan label, Listing 18
        gray = int(float(x_max - A[ai]) / float(x_max - x_min) * 100.)
        fill = 'gray' + str(gray)
        font = 'black'

        # Mengubah warna font menjadi putih apabila nilai gray kecil
        if gray < 50:
            font = 'white'

        # Menambahkan node dengan style tertentu
        G.add_node(ai, label=text, style='filled', fillcolor=fill, fontcolor=font)

    # Mengatur ketebalan edge, Listing 15
    values = [F[ai][aj] for ai in F for aj in F[ai]]
    x_min = min(values)
    x_max = max(values)

    # Menentukan batas minimum dan maksimum dari ketebalan edge
    y_min = 1.0
    y_max = 5.0

    for ai in F:
        for aj in F[ai]:
            # Mengambil nilai transition
            x = F[ai][aj]

            # Menghitung nilai ketebalan edge dari transition x
            y = y_min + (y_max - y_min) * float(x - x_min) / float(x_max - x_min)

            # Menambahkan edge dengan label x dan ketebalan y
            G.add_edge(ai, aj, label=x, penwidth=y)

    G.draw(file_path, prog='dot')
