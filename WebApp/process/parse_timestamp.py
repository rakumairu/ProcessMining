import datetime
import pygraphviz as pgv


def create_parse_timestamp(log, file_path):
    # Listing 32
    D = dict()
    for case_id in log:
        for i in range(0, len(log[case_id])-1):
            (ai, _, _, ti) = log[case_id][i]
            (aj, _, _, tj) = log[case_id][i+1]
            if ai not in D:
                D[ai] = dict()
            if aj not in D[ai]:
                D[ai][aj] = []
            D[ai][aj].append(tj-ti)

    for ai in sorted(D.keys()):
        for aj in sorted(D[ai].keys()):
            sum_td = sum(D[ai][aj], datetime.timedelta(0))
            count_td = len(D[ai][aj])
            avg_td = sum_td/count_td
            avg_td -= datetime.timedelta(microseconds=avg_td.microseconds)
            D[ai][aj] = avg_td
            print(ai, '->', aj, ':', D[ai][aj])

    G = pgv.AGraph(strict=False, directed=True)

    G.graph_attr['rankdir'] = 'TB'
    G.node_attr['shape'] = 'box'

    values = [D[ai][aj].total_seconds() for ai in D for aj in D[ai]]
    x_min = min(values)
    x_max = max(values)

    y_min = 1.0
    y_max = 5.0

    for ai in D:
        for aj in D[ai]:
            x = D[ai][aj].total_seconds()
            y = y_min + (y_max-y_min) * float(x-x_min) / float(x_max-x_min)
            G.add_edge(ai, aj, label=D[ai][aj], penwidth=y)

    G.draw(file_path, prog='dot')
