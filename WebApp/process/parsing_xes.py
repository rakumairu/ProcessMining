import datetime
import csv
import xml.etree.ElementTree as ET


def parsing_xes(xes_file):
    tree = ET.parse(xes_file)
    root = tree.getroot()

    data_list = []
    # data_list_O = []
    # data_list_W = []

    ns = {'xes': 'http://www.xes-standard.org/'}

    for trace in root.findall('xes:trace', ns):
        case_id = ''
        for string in trace.findall('xes:string', ns):
            if string.attrib['key'] == 'concept:name':
                case_id = string.attrib['value']
        for event in trace.findall('xes:event', ns):
            task = ''
            user = ''
            event_type = ''
            for string in event.findall('xes:string', ns):
                if string.attrib['key'] == 'concept:name':
                    task = string.attrib['value']
                if string.attrib['key'] == 'org:resource':
                    user = string.attrib['value']
                if string.attrib['key'] == 'lifecycle:transition':
                    event_type = string.attrib['value']
            timestamp = ''
            for date in event.findall('xes:date', ns):
                if date.attrib['key'] == 'time:timestamp':
                    timestamp = date.attrib['value']
                    timestamp = datetime.datetime.strptime(timestamp[:-10], '%Y-%m-%dT%H:%M:%S')

            temp = {'case_id': case_id, 'task': task, 'event_type': event_type, 'user': user, 'timestamp': str(timestamp)}
            print(temp)
            data_list.append(temp)
            # if temp.get('task')[0] == 'A':
            #     data_list_A.append(temp)
            # elif temp.get('task')[0] == 'W':
            #     # Only completed task that will be added
            #     data_list_W.append(temp)
            #     print(temp)
            #     if temp.get('event_type') == 'COMPLETE':
            #         pass
            # elif temp.get('task')[0] == 'O':
            #     data_list_O.append(temp)

    with open('./static/data/event.csv', 'w') as file:
        header = ['case_id', 'task', 'event_type', 'user', 'timestamp']
        writer = csv.DictWriter(file, header)

        writer.writeheader()
        for line in data_list:
            writer.writerow(line)

    # # Creating csv file for dataset with prefix A_
    # with open('./static/data/datasetA.csv', 'w') as file:
    #     header = ['case_id', 'task', 'event_type', 'user', 'timestamp']
    #     writer = csv.DictWriter(file, header)
    #
    #     writer.writeheader()
    #     for line in data_list_A:
    #         writer.writerow(line)
    #
    # # Creating csv file for dataset with prefix W_
    # with open('./static/data/datasetW.csv', 'w') as file:
    #     header = ['case_id', 'task', 'event_type', 'user', 'timestamp']
    #     writer = csv.DictWriter(file, header)
    #
    #     writer.writeheader()
    #     for line in data_list_W:
    #         print(line)
    #         writer.writerow(line)
    #
    # # Creating csv file for dataset with prefix O_
    # with open('./static/data/datasetO.csv', 'w') as file:
    #     header = ['case_id', 'task', 'event_type', 'user', 'timestamp']
    #     writer = csv.DictWriter(file, header)
    #
    #     writer.writeheader()
    #     for line in data_list_O:
    #         writer.writerow(line)
