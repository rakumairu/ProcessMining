"""
    Reading event log as dictionary and as list
"""

import csv, datetime


def as_dictionary(file):
    """
    Fungsi yang digunakan untuk membaca file csv yang berisi event log (case_id, task, user, timestamp) yang kemudian
    dimasukkan kedalam variabel bertipe dictionary
    :param file: file csv yang akan dibaca
    :return log: dictionary dari data yang dibaca dari file csv
    """

    log = dict()

    with open(file, newline='') as csv_file:
        f = csv.reader(csv_file, delimiter=',', quotechar='|')
        next(f)
        for line in f:
            # Membaca data case_id, task, user dari line yang terpisah oleh ;
            case_id = line[0]
            task = line[1]
            event_type = line[2]
            user = line[3]
            timestamp = line[4]

            # Mengecek dictionary log dengan key case_id, apabila masih kosong maka pada key tersebut dibuat list
            if case_id not in log:
                log[case_id] = []

            # Membuat tuple event yang terdiri dari task, user dan timestamp
            event = (task, event_type, user, timestamp)
            log[case_id].append(event)

    return log


def as_list(file):
    """
    Fungsi yang digunakan untuk membaca file csv yang berisi event log (case_id, task, user, timestamp) yang kemudian
    dimasukkan kedalam variabel bertipe list
    :param file: file csv yang akan dibaca
    :return log: list dari data yang dibaca dar file csv
    """

    log = []

    with open(file, newline='') as csv_file:
        f = csv.reader(csv_file, delimiter=',', quotechar='|')
        next(f)
        for line in f:
            # Membaca data case_id, task, user dari line yang terpisah oleh ;
            case_id = line[0]
            task = line[1]
            event_type = line[2]
            user = line[3]
            timestamp = line[4]

            # Membuat tuple event yang terdiri dari task, user dan timestamp
            event = (case_id, event_type, task, user, timestamp)
            log.append(event)

    return log


def as_dictionary_timestamp(file):
    """
    Fungsi yang digunakan untuk membaca file csv yang berisi event log (case_id, task, user, timestamp) yang kemudian
    dimasukkan kedalam variabel bertipe dictionary
    :param file: file csv yang akan dibaca
    :return log: dictionary dari data yang dibaca dari file csv
    """

    log = dict()

    with open(file, newline='') as csv_file:
        f = csv.reader(csv_file, delimiter=',', quotechar='|')
        next(f)
        for line in f:
            # Membaca data case_id, task, user dari line yang terpisah oleh ;
            case_id = line[0]
            task = line[1]
            event_type = line[2]
            user = line[3]
            timestamp = datetime.datetime.strptime(line[4], '%Y-%m-%d %H:%M:%S')

            # Mengecek dictionary log dengan key case_id, apabila masih kosong maka pada key tersebut dibuat list
            if case_id not in log:
                log[case_id] = []
                # print(line)

            # Membuat tuple event yang terdiri dari task, user dan timestamp
            event = (task, event_type, user, timestamp)
            log[case_id].append(event)

    return log
